import {
  Component,
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes
} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <div class="row">
    <div class="columns">
        <button (click)="toggleMove()">Press me for animation</button>
    </div>
    <div class="columns">
        <div id="content" [@focusPanel]='state'>Look at me animate</div>
    </div>
  </div>
  `,
  styles:[`
  button {font-size:1.8em; }
  #content{ padding:30px; background:#eeeeee}
  `],
  animations:[

    trigger('focusPanel',[
      state('inactive', style({
        transform: 'scale(1)'
      })),
      state('active', style({
        transform: 'scale(1.1)'        
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out'))      
    ])


  ]
})
export class AppComponent {

  state: string = 'inactive';

  toggleMove(){
    this.state = (this.state === 'inactive' ? 'active' : 'inactive');  
  }

}
